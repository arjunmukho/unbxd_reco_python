class Params:

    def setBoxType(self, boxType):
        self._boxType=boxType
    
    def getBoxType(self):
        if hasattr(self,"_boxType"):
            return self._boxType
        return None
    
    def setUserId(self,userId):
        self._userId = userId

    def getUserId(self):
        if hasattr(self, "_userId"):
            return self._userId
        return None

    def setProdId(self,prodId):
        self._ProdId = prodId

    def getProdId(self):
        if hasattr(self, "_prodId"):
            return self._prodId
        return None
    
    def getCategory(self):
        if hasattr(self, "_category"):
            return self._category
        return None

    def setCategory(self,category):
        self._category = category
    
    def getBrand(self):
        if hasattr(self, "_brand"):
            return self._brand
        return None

    def setBrand(self, brand):
        self._brand = brand
    
    def getLimit(self):
        if hasattr(self, "_limit"):
            return self._limit
        return None

    def setLimit(self, limit):
        self._limit = limit

    def getRegion(self):
        if hasattr(self, "_region"):
            return self._region
        return None

    def setRegion(self, region):
        self._region = region

    def setVersion(self,version):
        self._version = version

    def getVersion(self):
        if hasattr(self, "_version"):
            return self._version
        return None

    def setIp(self,ip):
        self._ip = ip

    def getIp(self):
        if hasattr(self, "_ip"):
            return self._ip
        return None

    def getFormat(self):
        if hasattr(self, "_format"):
            return self._format
        return None
    
    def setFormat(self,format):
        self._format=format  