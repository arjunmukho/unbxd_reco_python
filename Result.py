from UnbxdException import UnbxdException
class Result:
	def getRecommendations(self):
		if hasattr(self,"_recommendations"):
			return self._recommendations
		return None

	def setRecommendations(self,recommendations):
		self._recommendations = recommendations
	
	def getCount(self):
		if hasattr(self, "_count"):
			return self._count
		return None
	
	def setCount(self,count):
		self._count = count
	
	def getError(self):
		if hasattr(self, "_error"):
			return self._error
		return None
	
	def setError(self,error):
		self._error = error
	
	def getBoxType(self):
		if hasattr(self, "_boxType"):
			return self._boxType
		return None
	
	def setBoxType(self,boxType):
		self._boxType = boxType
	
	def __init__(self,response):
		try:
			self._response = response
			if not(self._response == None):
				if "count" in response:
					self._count=response["count"]
				if "Recommendations" in response:
					self._recommendations = response["Recommendations"]
				if "error" in response:
					self._error= response["error"]
				if "boxType" in response:
					self._boxType=response["boxType"]
				
		except Exception as e:
			#raise UnbxdException(e)
			print e