from UnbxdException import UnbxdException
from Result import Result
import httplib2
import json

class Service :

	def getRecommendedProducts(self, params):
		try:	
			if not(hasattr(params,"_ip")):
				raise UnbxdException("Ip Address is manadatory for Recommend BoxType")
			if not(hasattr(params, "_userId")):
				raise UnbxdException("UserId is mandatory for Recommend BoxType")

			url="?"+"ip="+params.getIp()+"&uid="+params.getUserId()
			return url 
		except Exception as e:
			raise UnbxdException(e)

	def getRecentlyViewedProducts(self, params):
		try:	
			if not(hasattr(params, "_userId")):
				raise UnbxdException("UserId is mandatory for Recommend BoxType")

			url="?"+"uid="+params.getUserId()
			return url
		except Exception as e:
			raise UnbxdException(e)

	def getAlsoViewedProducts(self, params):
		try:
			if not(hasattr(params, "_prodId")):
				raise UnbxdException("UserId is mandatory for Recommend BoxType")

			url="?"+"pids="
			prodIds=params.getProdId()
			for i in prodIds:
				url+=i+","
			return url
		except Exception as e:
			raise UnbxdException(e)

	def getAlsoBoughtProducts(self, params):
		try:
			if not(hasattr(params, "_prodId")):
				raise UnbxdException("UserId is mandatory for Recommend BoxType")

			url="?"+"pids="
			prodIds=params.getProdId()
			for i in prodIds:
				url+=i+","
			return url
		except Exception as e:
			raise UnbxdException(e)

	def getAlsoAddedToCartProducts(self, params):
		try:
			if not(hasattr(params, "_prodId")):
				raise UnbxdException("UserId is mandatory for Recommend BoxType")

			url="?"+"pids="
			prodIds=params.getProdId()
			for i in prodIds:
				url+=i+","
			return url
		except Exception as e:
			raise UnbxdException(e)

	def getMoreLikeTheseProducts(self, params):
		try:
			if not(hasattr(params, "_prodId")):
				raise UnbxdException("UserId is mandatory for Recommend BoxType")

			url="?"+"pids="
			prodIds=params.getProdId()
			for i in prodIds:
				url+=i+","
			return url
		except Exception as e:
			raise UnbxdException(e)

	def getTopSellersProducts(self, params):
		try:
			url=""
			if not(hasattr(params, "_category") or hasattr(params, "_brand")):
				raise UnbxdException("Either category or brand must be set for Top Seller BoxType")
			elif(hasattr(params, "_category")):
				url+="category-"+params.getCategory()+"/"
			else:
				url+="brand-"+params.getBrand()+"/";

			return url
		except Exception as e:
			raise UnbxdException(e)

	def prepareUrl(self, params, address):
		try:
			if not (hasattr(params, "_boxType")):
				raise UnbxdException("BoxType is a mandatory field")	
			
			boxType=params.getBoxType()
			url=address+boxType+"/"
			
			if boxType == "top-sellers":
				url+=self.getTopSellersProducts(params)
			elif  boxType == "recommend":
				url+=self.getRecommendedProducts(params) 
			elif boxType == "recently-viewed": 
				url+=self.getRecentlyViewedProducts(params)
			elif boxType == "also-viewed":
				url+=self.getAlsoViewedProducts(params)
			elif boxType == "also-added-to-cart":
				url+=self.getAlsoAddedToCartProducts(params)
			elif boxType == "also-bought":
				url+=self.getAlsoBoughtProducts(params)
			elif boxType == "more-like-these":
				url+=self.getMoreLikeTheseProducts(params)
			else:
				raise UnbxdException("Not a valid boxType ")

			if not(url[-1:] == "/"):
				url+="&"
			else:
				url+="?"
			url+="format="+params.getFormat()
			return url
		
		except Exception as e:
			print e
			raise UnbxdException(str(e))
		 
	
	def recommend(self, params, address):
		
		 try:
			url = self.prepareUrl(params, address)
			print url
			response, content = httplib2.Http().request(url)
			if (response['status'] != "200") :
				raise UnbxdException("Method failed: " + response['status'])
			
			result = json.loads(content)
			result = Result(result)
			return result
			
		 except Exception as e:
		 	print e
			raise UnbxdException(str(e))