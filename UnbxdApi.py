from UnbxdException import UnbxdException
from Params import Params
from Service import Service
class UnbxdApi:
	def __init__(self, siteName=None, apiKey=None, boxType=None, transport="http", region="apac", version ="v1.0"):
		try:
			self._params = Params()
			self.setSiteName(siteName)
			self.setApiKey(apiKey)
			self.setTransport(transport)
			self.setRegion(region)
			self.setVersion(version)
			self.setBoxType(boxType)
			self.setFormat("html")
		except Exception as e:
			raise UnbxdException(str(e))
	
	def getRegion(self):
		return self._params.getRegion()
	
	def setRegion(self,region):
		self._params.setRegion(region)	

	def getVersion(self):
		return self._params.getVersion()
	
	def setVersion(self,version):
		self._params.setVersion(version)	

	def getBoxType(self):
		return self._params.getBoxType()
	
	def setBoxType(self,boxType):
		self._params.setBoxType(boxType)
	
	def getUserId(self):
		return self._params.getUserId()
	
	def setUserId(self,userId):
		self._params.setUserId(userId)

	def getProdId(self):
		return self._params.getProdId()
	
	def setProdId(self,prodId):
		self._params.setProdId(prodId)	

	def getCategory(self):
		return self._params.getCategory()
	
	def setCategory(self,category):
		self._params.setCategory(category)	

	def getBrand(self):
		return self._params.getBrand()
	
	def setBrand(self,brand):
		self._params.setBrand(brand)	

	def getSiteName(self):
		return self._siteName
	
	def setSiteName(self,siteName):
		if(siteName == None):
			raise UnbxdException(UnbxdException.getNOSITENAME())
		self._siteName = siteName
	
	def getApiKey(self):
		return self._apiKey
	
	def setApiKey(self,apiKey):
		if apiKey == None :
			raise UnbxdException(UnbxdException.getNOAPIKEY())
		self._apiKey = apiKey
	
	def  getTransport(self):
		return self._transport
	
	def setTransport(self, transport):
		self._transport = transport

	def getFormat(self):
		return self._params.getFormat()
	
	def setFormat(self,format):
		self._params.setFormat(format)	

	def getResults(self) :
		address=""
		address+= self.getTransport()+"://"+ self._params.getRegion()+ "-recommendations.unbxdapi.com/"+self._params.getVersion()+"/"+self.getApiKey()+"/"+self.getSiteName()+"/"
		unbxdServiceObject = Service()
		response = unbxdServiceObject.recommend(self._params, address)
		return response