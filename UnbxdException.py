class UnbxdException(Exception):
	def getNOUSERID(self):
		return self._NOUSERID
	

	def setNOUSERID(self, nORESPONSE):
		self._NOUSERID = nORESPONSE
	

	def getNOPRODID (self):
		return self._NOPRODID  
	

	def setNOPRODID (self, NOPRODID):
		self._NOPRODID  = NOPRODID  
	
	def getNOBOXTYPE (self):
		return self._NOBOXTYPE  
	

	def setNOBOXTYPE (self, NOBOXTYPE ):
		self._NOBOXTYPE  = NOBOXTYPE  
	
	def getNOTVALIDBOXTYPE (self):
		return self._NOTVALIDBOXTYPE  
	
	def setNOTVALIDBOXTYPE (self, NOTVALIDBOXTYPE ):
		self._NOTVALIDBOXTYPE  = NOTVALIDBOXTYPE  
	
	def getNOCATEGORYBRAND (self):
		return self._NOCATEGORYBRAND  
	

	def setNOCATEGORYBRAND (self, NOCATEGORYBRAND ):
		self._NOCATEGORYBRAND  = NOCATEGORYBRAND  
	

	def getNORESPONSE(self):
		return self._NORESPONSE 
	

	def setNORESPONSE(self, nORESPONSE):
		self._NORESPONSE = nORESPONSE 
	
	def getNORESPONSE(self):
		return self._NORESPONSE 
	

	def setNORESPONSE(self, nORESPONSE):
		self._NORESPONSE = nORESPONSE 
	

	def getNOSITENAME(self):
		return self._NOSITENAME 
	

	def setNOSITENAME( self,nOSITENAME):
		self._NOSITENAME = nOSITENAME 
	

	def getNOAPIKEY(self):
		return self._NOAPIKEY 
	

	def setNOAPIKEY( self,nOAPIKEY):
		self._NOAPIKEY = nOAPIKEY 

	def getErrorMessage(self):
		return self._errorMessage
	def __init__(self, errorMessage=None, errorCode= None):
		self._errorMessage = errorMessage
		self._NORESPONSE = "Couldn't connect to Unbxd" 
		self._NOSITENAME = "Site name cannot be null" 
		self._NOAPIKEY = "Api key cannot be null" 
		self._NOBOXTYPE= "BoxType must be specified"
		self._NOTVALIDBOXTYPE= "BoxType must be valid"
		self._NOUSERID= "BoxType must be valid"
		self._NOPRODID= "BoxType must be valid"
		self._NOCATEGORYBRAND= "Category or Brand must be specified for this BoxType"
		self._errorCode = errorCode